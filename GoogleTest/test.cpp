#include "pch.h"
#include "../SDL2VisualStudioProject/GameCode/components/Grid.hpp"
namespace GridTest {
	TEST(GridValidMoveTest, GridTest1) {
		std::shared_ptr<Grid> grid = Grid::createWithSizeAndType(3, 3, 3, 1);
		EXPECT_TRUE(grid->isPossibleMoves());
		//EXPECT_EQ(1, 1);
		//EXPECT_TRUE(true);
	}

	TEST(GridValidMoveTest, GridTest2) {
		std::shared_ptr<Grid> grid = Grid::createWithSizeAndType(3, 3, 3, 1);
		grid->setGridManually(GameBoard{ {1,2,3}, {5,6,7}, {9,8,1} });
		EXPECT_FALSE(grid->isPossibleMoves());
		//EXPECT_EQ(1, 1);
		//EXPECT_TRUE(true);
	}

	TEST(GridValidMoveTest, GridTest3) {
		std::shared_ptr<Grid> grid = Grid::createWithSizeAndType(100, 100, 100, 42);
		EXPECT_TRUE(grid->isPossibleMoves());
		//EXPECT_EQ(1, 1);
		//EXPECT_TRUE(true);
	}

	TEST(GridValidMoveTest, GridTest4) {
		std::shared_ptr<Grid> grid = Grid::createWithSizeAndType(3, 3, 3, 1);
		grid->setGridManually(GameBoard{ {1,2,3}, {1,3,3}, {2,1,1} });
		EXPECT_FALSE(grid->isValidMove(2,0));
		//EXPECT_EQ(1, 1);
		//EXPECT_TRUE(true);
	}
}