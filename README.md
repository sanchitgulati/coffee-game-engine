# Coffee Game Engine with Match 3 game

Home exercise to write a modern c++ 2d game engine on SDL and make it dev friendly for game development. 
Sample Match-3 game to demonstrate.


## Gif

[![gif](https://bitbucket.org/sanchitgulati/coffee-game-engine/raw/22674a13bd9d07e7eb941970ef46d9bf57fb39e2/docs/demo_coffee.gif)]()


## Features
 - 2D Camera based custom pipe GL renderer
 - Scene Manager
 - Event System
 - Inheritable and extendable GameObject
 - Tween Sequence System
 - Google Test Framework integration


## Third Party Libs
* GLEW
* GLM
* gTest
* SDL2
* SDL2-TTF
* SDL2-mixer
* SDL2-image


## Few How-To’s
---

#### Adding GameObjects to Scene
```c++
auto background = Sprite::createWithFile(GameValues::BG);
background->setPosition(WIN_WIDTH *0.5f, WIN_HEIGHT * 0.5f);
addChild(background);
```

#### Chainable sequence system

```c++
makeSequence()
->add(SCALE_TO(shared_from_this(), 1.f, 0.9f, 0.1f))
->add(FUNC_CALL([this]() {
AudioManager.playAudio(GameValues::sfxCLICK);
_pressedState->setVisible(true); }))
->add(SCALE_TO(shared_from_this(), 0.9f, 1.1f, 0.1f))
->add(SCALE_TO(shared_from_this(), 1.1f, 1.f, 0.1f))
->add(DELAY(.5f))
->add(FUNC_CALL([this]() {
std::invoke(_callback);
}));
```


Credits

Music : [Hope - ROFEU](https://www.youtube.com/watch?v=W-w1hVLvIJw) 
Art : [Game Art Guppy](https://www.gameartguppy.com/) 

