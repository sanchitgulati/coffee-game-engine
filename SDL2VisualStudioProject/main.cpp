#include <stdio.h>
#include <iostream>
#include "Coffee/Coffee.hpp"
#include "GameCode/MainMenu.hpp"
#include "GameCode/GameOverScene.hpp"


using namespace Coffee;
int main(int argc, char* args[])
{
	Application application;
	application.Init("Caffinism","Match-3", WIN_WIDTH, WIN_HEIGHT);

	application.GetSceneManager()->setScene(MainMenu::create());
	application.GameLoop();
	application.GameCleanup();
	return 0;
}